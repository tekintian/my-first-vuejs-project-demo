/*
* @Author: tekin
* @Date:   2017-07-22 21:38:58
* @Last Modified 2017-07-22
*/

const STORAGE_KEY = 'test-vuejs'
export default {
  fetch: function () {
    return JSON.parse(window.localStorage.getItem(STORAGE_KEY) || '[]')
  },
  save: function (items) {
    window.localStorage.setItem(STORAGE_KEY, JSON.stringify(items))
  }
}
